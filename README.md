Wintersmith categories pages plugin
===

This plugin will take articles in a wintersmith website and sort them per categories.

## Features

- categories: the plugin will read the content in the `articles` part of the wintersmith content tree (configurable), pick up `category` metadata from all the items found, and sort them into pages per category. The pages are also paginated.

- "production" build: - the plugin will also check if your `NODE_ENV === 'prod'`, and if yes, it will skip the articles having a metadata flag `draft` set to true. That way you can look at what you wrote in `wintersmith preview`, but it will not end up in your final build.

## Usage

- Install the plugin:


    npm install @zladuric/wintersmith-categories

- Add it to your `config.json`:


    {
        "plugins": [
        "@zladuric/wintersmith-categories"
        ]
    }

- Add _category page_ and _categories home_ template.

Add a category page and categories home page template. By default, we want `category.pug` and `categories.pug`. The pages can use `categories` array of categories and `articles` collection of articles in the given category.

An example category page could look like this:


    extends layout
    
    block content
      include author
      .search
        h1= 'Category:' + category
    
      .posts
        each article in articles
        // render part or the whole article


The categories page could look like this:


    extends layout
    
    block content
      include author
      .search
        h2 Categories
      .posts
        each category in categories
          section.post
            header.post-header
                h2.post-title
                  a.title-link(href=category)= category


- (Optionally) configure the plugin

Append this to your `config.json`:

```
{
    "categories": {
        "template": "category.pug", // template that renders pages
        "categoriesHomeTemplate": "categories.pug", // template for categories home
        "articles": "articles", // directory containing contents to paginate
        "validTemplates": ["article.pug", "note.pug"], // which article templates we're working here with
        "categoriesHomeFilename": "category/index.html", // Listing of all categories
        "perPage": 10 // number of articles per page
    } 
}
```


## Motivation

I'm using [wintersmith](https://wintersmith.io) for my website and wanted this feature. I also need it published as a separate npm package. There wasn't any suitable, so I've built my own. I'm putting it on npm in case someone else finds it a useful example.    
