#!/usr/bin/env node
const arg = process.argv[2];

const what = arg === 'cat' || arg === 'c' ? 'cat' :
  ( arg === 'index' || arg === 'i' ? 'index' :
      (arg === 'all' || arg === 'both' ? 'all' : null)
  );
if (!what) {
  usage();
  process.exit(1);
} else if (what === 'cat') {
  console.log(getCategoryPage());
} else if (what === 'index') {
  console.log(getCategoriesPage());
} else if (what === 'all') {
  console.log(`Single category page template:

${getCategoryPage()}

Categories Index page template:

${getCategoriesPage()}
`);
} else {
  usage();
}

function getCategoryPage() {
    const content = `extends layout

block content
  include author
  .search
    h1= 'Category:' + category

  .posts
    each article in articles
      // <h1 class="content-subhead">Pinned Post</h1>
      // A single blog post
      section.post
        header.post-header
          if article.metadata.type != 'note'
            h2.post-title
              a.title-link(href=article.url)= article.title
          p.post-meta
            | Under
            // a.post-category(href='category.html#' + article.metadata.category)= article.metadata.category
            // TODO fix category/finish search for this to work
            span.post-category= article.metadata.category
            | on
            span= moment.utc(article.date).format('DD. MMMM YYYY')
        .post-description
          if article.intro.length > 0
            != typogr(article.intro).typogrify()
            p.more
              a(href=article.url) more

  // Before the end, we need to place page nav.
  .nav
    if prevPage
      a(href=prevPage.url) &laquo; Newer
    else
      a(href='/archive.html') &laquo; Archives
    if nextPage
      a(href=nextPage.url) Next page &raquo;`;
  return content;
}

function getCategoriesPage() {
  const content = `
extends layout

block content
  include author
  .search
    h2 Categories
  .posts
    each category in categories
      section.post
        header.post-header
            h2.post-title
              a.title-link(href=category)= category
`
  return content;

}

function usage() {
  console.log(`wintersmith-category plugin template generator. 

To get the basic template contents for the category page, run "wcp cat" or "wcp c".
To get the categories index page, run "wcp index" or "wcp i".
To get both, run "wcp both" or "wcp all".

Example:

    wcp cat > templates/category-page.pug   # Generates the category page template in your source tree
    wcp all                                 # Generates all template pages and displays them.
`);
}


