module.exports = function categoryPageBuilder(options, Page) {
  class CategoryPage extends Page {
    constructor(categories) {
      super();
      this.categories = categories;
    }

    getFilename() {
      return options.categoriesHomeFilename
    }

    getView() {
      return function (env, locals, contents, templates, callback) {
        const template = templates[options.template];
        if (template === null) {
          return callback(new Error(`unknown categories home template '${ options.template }'`));
        }
        // setup the template context
        const ctx = {
          categories: this.categories,
        };

        // extend the template context with the environment locals
        env.utils.extend(ctx, locals);

        // finally render the template
        console.log('Building the thing');
        console.log()
        return template.render(ctx, callback);
      }
    }
  }
  return CategoryPage;
}
