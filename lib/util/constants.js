const CATEGORY_FILENAME_PLACEHOLDER = '%CATEGORY%';
const PAGE_NUM_PLACEHOLDER = '%PAGE_NUM%';
const PLUGIN_NAME = 'category';

/* */

/**
 * Category plugin defaults. Defaults can be overridden in config.json
 * e.g. "categoryPage": { "perPage": 10 }
 */
const pluginDefaults = {
  template: 'category.pug', // template that renders pages
  categoriesHomeTemplate: 'categories.pug', // template for categories home
  articles: 'articles', // directory containing contents to paginate
  validTemplates: ['article.pug'], // which article templates we're working here with
  categoriesHomeFilename: 'category/index.html', // Listing of all categories
  firstPageFilename: `category/${CATEGORY_FILENAME_PLACEHOLDER}/index.html`, // filename/url for first page
  filename: `category/${CATEGORY_FILENAME_PLACEHOLDER}/${PAGE_NUM_PLACEHOLDER}/index.html`, // filename for rest of pages
  perPage: 10 // number of articles per page
};

module.exports = {
  pluginDefaults,
  PLUGIN_NAME,
};
