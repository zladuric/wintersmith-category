const getValidArticles = require('./get-valid-articles');

/**
 *
 * @param contents {Object} wintersmith content tree directory
 * @param options wintersmith options
 * @param isProd {boolean} "production" build flag
 * @returns {string[]}
 */
module.exports = function getCategories(contents, options, isProd) {
  const articles = getValidArticles(contents, options, isProd);
  const categories = articles
    .map(article => article.metadata.category)
    .map(category => category.toLowerCase());
  // Make unique
  return [...new Set(categories)];
}
