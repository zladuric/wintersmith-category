/**
 *
 * @param contents {Object} wintersmith content tree
 * @param articles {Object[]} content tree collection
 * @param validTemplates {string[]} an array of valid template types.
 * @param isProd {boolean} "production" build flag
 * @returns {Object[]}
 */
module.exports = function getValidArticles(contents, { articles, validTemplates }, isProd) {
  // get all items out of our directories
  const validArticles = contents[articles]._.directories.map(item => item.index)
    // filter out articles with invalid template
    .filter(article => validTemplates.includes(article.template));
  // in prod environment, filter out draft articles
  if (isProd) {
    return validArticles.filter(article => !article.metadata.draft);
  }
  return validArticles;
};
